import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mbriskar on 10/7/15.
 */
public class Main
{

    public static void main(String[] args) {
        Map<String,Ticker> tickers = new HashMap<String, Ticker>();
        tickers.put("TEA", new Ticker().setTicker("TEA").setType(TickerTypeEnum.COMMON).setLastDivident(0).setParValue(100).setQuantity(50).setFixedDivident(0.03));
        tickers.put("ALE",new Ticker().setTicker("ALE").setType(TickerTypeEnum.COMMON).setLastDivident(30).setParValue(130).setQuantity(
                    30).setFixedDivident(0.01));
        tickers.put("POP", new Ticker().setTicker("POP").setType(TickerTypeEnum.COMMON).setLastDivident(0).setParValue(100).setQuantity(50));
        tickers.put("GIN",new Ticker().setTicker("GIN").setType(TickerTypeEnum.COMMON).setLastDivident(0).setParValue(100).setQuantity(10));
        tickers.put("JOE", new Ticker().setTicker("JOE").setType(TickerTypeEnum.COMMON).setLastDivident(0).setParValue(100).setQuantity(30));

        List<Trade> trades = new ArrayList<Trade>();
        trades.add(new Trade().setQuantity(2).setStockPrice(50).setTickerString("ALE"));
        trades.add(new Trade().setQuantity(2).setStockPrice(50).setTickerString("TEA"));
        trades.add(new Trade().setQuantity(2).setStockPrice(50).setTickerString("TEA"));
        trades.add(new Trade().setQuantity(2).setStockPrice(50).setTickerString("TEA"));

        for (String tickerKey : tickers.keySet())
        {
            Ticker t =tickers.get(tickerKey);
            double tickerPrice = StockCalculator.tickerPrice(tickerKey, trades);
            if(tickerPrice != 0) {
                System.out.println(tickerKey + ": ");
                double dividentYield = StockCalculator.dividentYield(t, tickerPrice);
                double peRatio =  StockCalculator.peratio(tickerPrice,t);
                System.out.println("Divident yield: " + dividentYield);
                System.out.println("p/e ratio : " + peRatio);
                if(tickerPrice < t.getParValue()) {
                    System.out.println("Buy " + 10 + " shares of " + tickerKey + " now " + new Date());
                } else {
                    System.out.println("Sell " + 10 + " shares of " + tickerKey + " now " + new Date());
                }
            }
        }


    }


}

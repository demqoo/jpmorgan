public class Ticker
{
    private String ticker;
    private TickerTypeEnum type;
    private int lastDivident;
    private double fixedDivident;
    private int parValue;
    private int quantity;

    public String getTicker()
    {
        return ticker;
    }

    public Ticker setTicker(String ticker)
    {
        this.ticker = ticker;
        return this;
    }

    public TickerTypeEnum getType()
    {
        return type;
    }

    public Ticker setType(TickerTypeEnum type)
    {
        this.type = type;
        return this;
    }

    public int getLastDivident()
    {
        return lastDivident;
    }

    public Ticker setLastDivident(int lastDivident)
    {
        this.lastDivident = lastDivident;
        return this;
    }

    public int getQuantity()
    {
        return lastDivident;
    }

    public Ticker setQuantity(int quantity)
    {
        this.quantity = quantity;
        return this;
    }

    public double getFixedDivident()
    {
        return fixedDivident;
    }

    public Ticker setFixedDivident(double fixedDivident)
    {
        this.fixedDivident = fixedDivident;
        return this;
    }

    public int getParValue()
    {
        return parValue;
    }

    public Ticker setParValue(int parValue)
    {
        this.parValue = parValue;
        return this;
    }
}

/**
 * Created by mbriskar on 10/10/15.
 */
public class Trade
{
    private int quantity;
    private int stockPrice;

    public String getTickerString()
    {
        return tickerString;
    }

    public Trade setTickerString(String tickerString)
    {
        this.tickerString = tickerString;
        return this;
    }

    private String tickerString;

    public int getQuantity()
    {
        return quantity;
    }

    public Trade setQuantity(int quantity)
    {
        this.quantity = quantity;
        return this;
    }

    public int getStockPrice()
    {
        return stockPrice;
    }

    public Trade setStockPrice(int stockPrice)
    {
        this.stockPrice = stockPrice;
        return this;
    }


}

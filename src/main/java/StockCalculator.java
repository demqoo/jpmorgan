import java.util.List;
import java.util.Map;

/**
 * Created by mbriskar on 10/7/15.
 */
public class StockCalculator
{
    public static double dividentYield(Ticker t,double tickerPrice) {
        double result =0;
        if(t.getType().equals(TickerTypeEnum.COMMON)) {
            result =  new Double(t.getLastDivident())/new Double(tickerPrice);
        } else {
            result =  new Double(t.getFixedDivident() * t.getParValue())/new Double(tickerPrice);
        }

        return result;
    }

    public static int tickerPrice(String s, List<Trade> trades) {
        int ammount = 0;
        for (Trade trade : trades)
        {
            if(trade.getTickerString().equals(s)) {
                ammount += trade.getQuantity() * trade.getStockPrice();
            }
        }
        return ammount;
    }

    public static float recordATrade(Ticker t) {
        return t.getParValue();
    }

    public static double peratio(double tickerPrice, Ticker t) {
        double percentage = t.getFixedDivident();
        double divident = t.getParValue() * percentage;
        return tickerPrice / divident;
    }
}
